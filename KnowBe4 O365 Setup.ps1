﻿﻿$UPNInput = Read-Host -Prompt 'Enter the email address of the account you want to setup'
Add-Type -AssemblyName 'System.Windows.Forms'
Start-Process "$env:APPDATA\Microsoft\Windows\Start Menu\Programs\Microsoft Corporation\Microsoft Exchange Online Powershell Module.appref-ms"
sleep 4
get-process -name powershell | format-list id | format-list id | Out-File -filepath C:\temp\knowbe4idresults.txt
Get-Content -Path C:\temp\knowbe4idresults.txt | Where-Object {$_ -like '*id*'} | Out-File -filepath C:\temp\knowbe4idresults2.txt
foreach($line in Get-Content C:\temp\knowbe4idresults2.txt) {
    if($line -ne $pid){
        $NewPSid = $line
    }
}

Add-Type -AssemblyName Microsoft.VisualBasic
Add-Type -AssemblyName System.Windows.Forms
Add-Type -AssemblyName PresentationFramework 
[Microsoft.VisualBasic.Interaction]::AppActivate([Int32]$NewPSid.ToString().Trim().Substring(5))
[System.Windows.Forms.SendKeys]::SendWait("Connect-EXOPSSession -UserPrincipalName $UPNInput~")
[System.Windows.MessageBox]::Show('Click OK once you have logged in and see a cursor')
[Microsoft.VisualBasic.Interaction]::AppActivate([Int32]$NewPSid.ToString().Trim().Substring(5))
[System.Windows.Forms.SendKeys]::SendWait('Enable-OrganizationCustomization~')
[System.Windows.MessageBox]::Show('Click OK once command is done and you see a cursor')
[Microsoft.VisualBasic.Interaction]::AppActivate([Int32]$NewPSid.ToString().Trim().Substring(5))
[System.Windows.Forms.SendKeys]::SendWait('Set-HostedConnectionFilterPolicy “Default” –IPAllowList 192.254.121.248,23.21.109.212,23.21.109.197~')
sleep 10
[System.Windows.Forms.SendKeys]::SendWait('new-transportrule -Name "Bypass spam for KnowBe4" -SenderAddressLocation Header -Mode Enforce -RuleErrorAction Ignore -RuleSubType None -HasNoClassification $False -HasSenderOverride $False -SenderIpRanges 23.21.109.212, 23.21.109.197, 192.254.121.248 -AttachmentIsUnsupported $False -AttachmentProcessingLimitExceeded $False -AttachmentHasExecutableContent $False -AttachmentIsPasswordProtected $False -ExceptIfHasNoClassification $False -ExceptIfAttachmentIsUnsupported $False -ExceptIfAttachmentProcessingLimitExceeded $False -ExceptIfAttachmentHasExecutableContent $False -ExceptIfAttachmentIsPasswordProtected $False -ExceptIfHasSenderOverride $False -SetSCL -1 -SetHeaderName X-MS-Exchange-Organization-BypassClutter -SetHeaderValue true -StopRuleProcessing $False -RouteMessageOutboundRequireTls $False -ModerateMessageByManager $False -DeleteMessage $False -Quarantine $False -ApplyOME $False -RemoveOME $False -RemoveOMEv2 $False~')
sleep 20
[System.Windows.Forms.SendKeys]::SendWait('new-transportrule -Name "Bypass junk for KnowBe4" -SenderAddressLocation Header -Mode Enforce -RuleErrorAction Ignore -RuleSubType None -HasNoClassification $False -HasSenderOverride $False -SenderIpRanges 23.21.109.212, 23.21.109.197, 192.254.121.248 -AttachmentIsUnsupported $False -AttachmentProcessingLimitExceeded $False -AttachmentHasExecutableContent $False -AttachmentIsPasswordProtected $False -ExceptIfHasNoClassification $False -ExceptIfAttachmentIsUnsupported $False -ExceptIfAttachmentProcessingLimitExceeded $False -ExceptIfAttachmentHasExecutableContent $False -ExceptIfAttachmentIsPasswordProtected $False -ExceptIfHasSenderOverride $False -SetHeaderName X-Forefront-Antispam-Report -SetHeaderValue SFV:SKI -StopRuleProcessing $False -RouteMessageOutboundRequireTls $False -ModerateMessageByManager $False -DeleteMessage $False -Quarantine $False -ApplyOME $False -RemoveOME $False -RemoveOMEv2 $False~')
sleep 20
Connect-MsolService
[System.Windows.Forms.SendKeys]::SendWait("Get-PSSession | Remove-PSSession~")
[System.Windows.Forms.SendKeys]::SendWait('exit~')


New-Item -ItemType directory -Path $env:temp\KB4O365
$pathToFolder = "$env:temp\KB4O365\"
Connect-MsolService
get-msoluser | Format-Table -Property UserPrincipalName,FirstName,LastName |Out-File -filepath "$pathToFolder\org_users.csv"

ii $pathToFolder
$Shell = New-Object -ComObject "WScript.Shell"
$Button = $Shell.Popup("Click OK to continue.", 0, "Open the org_users.csv file to remove any unwanted users and to verify everyone has a first and last name.", 0)

$content = Get-Content -Path "$pathToFolder\org_users.csv"
$content2 = ($content -replace(" ",",") -notmatch '(^[\s,-]*$)|(rows\s*affected)' -split "," | where{$_ -ne ""})-join "," 
$charArray = [char[]]$content2 
$charArray | Set-Content -path "$pathToFolder\users.csv"
$counter = 0
$newCharArray = New-Object System.Collections.ArrayList($null)
foreach($char in $charArray){
    if ($char -eq ","){
        $counter= $counter + 1
        if ($counter -eq 3){
            $newCharArray.Add("`r`n")
            $counter = 0    
        }else{
            $newCharArray.Add($char)
        } 
    }else{
        $newCharArray.Add($char)
    }
}
$content3= $newCharArray -join ""
$content3= $content3 -replace "UserPrincipalName","Email" -replace "FirstName"," First Name" -replace "LastName"," Last Name"
$content3 | Set-Content -path "$pathToFolder\users.csv"

ii $pathToFolder